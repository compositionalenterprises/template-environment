# Environment

These files are the environment and contain within them the files that are needed to complete the setup of the compositional role.

This repo is typically going to be cloned down in the base directory of the play, to the directory name `environment`.

```bash
$ git clone git@gitlab.com/smacz/environment.git
```

This name is hardcoded in many of the surrounding playbook files, so it's necessary to clone it down that way. However, the compositional role will work with the defaults, as long as `{{ environment_domain }}` is set somewhere.

Also, the `.vault_pass` file is conspicuously missing from this repo. That is definitely one of the things that will have to be set up once the repo is cloned. If possible, I'd love to set up a post hook that does this automatically once the repo is cloned down. It _does_ go in the base of this repo though. Just...right here.

This is meant to be the upstream for all other environments. Any defaults will be placed here, and it's expected that the other environments will rebase their changes as soon as any changes are made. In this way, it functions as the upstream of all other environment repos.

This is mean to be feasible to implement in a larger way across many different ansible setups, and is preferable to other ways of organization, as it allows for a modularity of testing across both applications and domains. But more on that later. For now, we are just getting it working with ourcompose and compositional.

# Quick Variables

  - `compositional_webroot_redirect` to set the root service
  - `compositional_{commandcenter,portal}_production_yml_enc` for rails encrypted yaml file
  - `compositional_{commandcenter,portal}_production_key` for rails key
